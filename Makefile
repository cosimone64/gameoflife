CXX = g++
CXXFLAGS = -std=c++17 -O3 -flto -Wl,-flto -Wall -Wextra -Weffc++ -Wpedantic\
	   -Wuninitialized -Werror -fno-rtti -fno-exceptions -fmax-errors=1
LIBS = -lsfml-graphics -lsfml-window -lsfml-system
LIFE = life


OBJS = $(LIFE).o

all: $(LIFE)

debug: CXXFLAGS := -std=c++17 -O0 -Og -g -D LIFE_DEBUG
debug: all

clean:
	rm -rf $(LIFE) $(OBJS)

$(LIFE): $(LIFE).o
	$(CXX) $(CXXFLAGS) $(LIBS) $(LIFE).o -o $(LIFE)

$(LIFE).o: $(LIFE).cpp
	$(CXX) $(CXXFLAGS) -c $(LIFE).cpp -o $(LIFE).o
