#include <algorithm>
#include <array>
#include <bitset>
#include <chrono>
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <SFML/Graphics.hpp>

// Import frequently used types into the global namespace
using std::uint8_t;
using std::vector;

/*
 * These constant parameters can be edited to suit your preferences.
 */
constexpr auto title = "Game of life";
constexpr auto frame_period = std::chrono::milliseconds {100};
// Colors are represented in RGBA format.
constexpr auto color_on = std::array<uint8_t, 4> {0x88, 0x00, 0x88, 0xFF};
constexpr auto color_off = std::array<uint8_t, 4> {0x00, 0x00, 0x00, 0xFF};
constexpr auto alive_char = '*';

using Matrix = vector<vector<bool>>;

class Display
{
	sf::RenderWindow window;
	sf::Texture      texture;
	sf::Sprite       sprite;

	void
	resize_window(int cols, int rows);
public:
	Display(const Matrix &matrix);

	void
	draw(const Matrix &matrix);
};

void die (const char *s) { std::cout << s << '\n'; std::exit(1);
}
bool
update_cell_state(const int i, const int j, const Matrix &matrix)
{
	const auto rows = matrix.size();
	const auto cols = matrix[0].size();
	const auto i_start = std::max(i - 1, 0);
	const auto j_start = std::max(j - 1, 0);
	const auto i_end = std::min(i + 2, static_cast<int>(rows));
	const auto j_end = std::min(j + 2, static_cast<int>(cols));
	auto neighbors = 0;

	for (auto ii = i_start; ii < i_end; ++ii) {
		for (auto jj = j_start; jj < j_end; ++jj) {
			neighbors += matrix[ii][jj];
		}
	}
	const auto is_alive = matrix[i][j];
	if (is_alive) {
		--neighbors;
	}
	if (is_alive &&  neighbors <  2) {
		return false;
	} else if (is_alive && (neighbors == 2 || neighbors == 3)) {
		return true;
	} else if (is_alive &&  neighbors >  3) {
		return false;
	} else if (!is_alive &&  neighbors == 3) {
		return true;
	} else {
		return is_alive;
	}
}
Matrix
update_matrix(const Matrix &matrix)
{
	const auto rows = matrix.size();
	const auto cols = matrix[0].size();
	auto new_matrix = Matrix {rows, vector<bool> (cols)};

	for (auto i = 0u; i < rows; ++i) {
		for (auto j = 0u; j < cols; ++j) {
			new_matrix[i][j] = update_cell_state(i, j, matrix);
		}
	}
	return new_matrix;
}
Display::Display(const Matrix &matrix)
	: window {sf::VideoMode {1, 1}, title}, texture {}, sprite {}
{
	const auto cols = matrix.size();
	const auto rows = matrix[0].size();
	if (!texture.create(cols, rows)) {
		die("Error while creating texture");
	}
	sprite.setTexture(texture);
}
void
Display::resize_window(const int cols, const int rows)
{
	auto ev = sf::Event {};
	while (window.pollEvent(ev) && ev.type == sf::Event::Resized) {
		window.setView(sf::View {{0, 0,
				static_cast<float>(ev.size.width),
				static_cast<float>(ev.size.height)}});
		sprite.setScale(window.getSize().x / cols,
				window.getSize().y / rows);
	}
}
void
Display::draw(const Matrix &matrix)
{
	const auto rows = matrix.size();
	const auto cols = matrix[0].size();
	window.clear();
	resize_window(cols, rows);

	auto pixels = vector<uint8_t> (rows * cols * 4);
	for (auto i = 0u; i < rows; ++i) {
		for (auto j = 0u; j < cols * 4; j += 4) {
			const auto &color =
				matrix[i][j / 4] ? color_on : color_off;
			for (auto k = 0; k < 4; ++k)
				pixels[i * cols * 4 + j + k] = color[k];
		}
	}
	texture.update(pixels.data());
	window.draw(sprite);
	window.display();
}
Matrix
init_matrix(const char *filename)
{
	auto matrix = Matrix {};
	auto file = std::ifstream {filename};
	auto max_cols = 0u;
	auto curr_row = 0u;
	auto curr_col = 0u;

	matrix.emplace_back();
	while (file.good()) {
		const auto curr_char = file.get();
		if (curr_char == '\n') {
			++curr_row;
			if (curr_col > max_cols) {
				max_cols = curr_col;
			}
			curr_col = 0;
			matrix.emplace_back();
		} else {
			++curr_col;
			matrix[curr_row].push_back(curr_char == alive_char);
		}
	}
	matrix.pop_back();
	for (auto &row : matrix) {
		for (auto i = row.size(); i < max_cols; ++i) {
			row.push_back(false);
		}
	}
	return matrix;
}
int
main(int argc, char *argv[])
{
	if (argc != 2) {
		std::cerr << "Use as " << argv[0] << " <filename>\n";
		return 1;
	}
	auto start_time = std::chrono::steady_clock::now();
	auto matrix = init_matrix(argv[1]);
	auto display = Display {matrix};
	auto is_open = true;

	while (is_open) {
		const auto current_time = std::chrono::steady_clock::now();
		auto delta_time = current_time - start_time;
		if (delta_time >= frame_period) {
			start_time = current_time;
		}
		const auto frames_to_render = delta_time / frame_period;
		if (!frames_to_render) {
			std::this_thread::sleep_for(frame_period);
		}
		for (auto i = frames_to_render; i > 0; --i) {
			matrix = update_matrix(matrix);
			display.draw(matrix);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
			is_open = false;
		}
	}
	return 0;
}
