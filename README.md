# gameoflife

My implementation of the Game of Life. Uses the SFML library for
graphics. (https://www.sfml-dev.org/). Make sure you have this
library installed to run the program.

The program reads the initial seed from a text file.

## Build

### Unix-like systems

Simply `cd` to the `src` directory and run `make`. The default
Makefile provided uses g++ as the default compiler.

### Windows

No build options yet, although it shouldn't be hard to do in the
future considering that the program does not rely on any OS-specific
behavior/library.

## Usage

Run as `./life <filename>`

## TODO

As of now, the matrix size is hard-coded (40x40).  In the future, I
will probably add the possibility to parse an initial configuration
file with arbitrary dimensions, removing these hard-coded constraints.
